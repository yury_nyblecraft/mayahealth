import React, { useEffect } from "react";
import * as Survey from "survey-react";
import * as widgets from "surveyjs-widgets";
import $ from "jquery";
import noUiSlider from "nouislider";
import "nouislider/distribute/nouislider.min.css";
import "survey-react/survey.css";
import "./MayaSurvey.css";

const surveyJSON = {
  title: "Patient Health Questionaree (PHQ)",
  description:
    "The Day of Session Preparation form is designed to be completed prior to beginning a session. The purpose of this form is to gather information about the client’s mental state, level of rest and nourishment, and attitude leading up to the session.",
  showQuestionNumbers: "off",
  pages: [
    {
      name: "page1",
      questions: [
        {
          type: "radiogroup",
          choices: ["Yes", "No"],
          isRequired: true,
          name: "physicalAliments",
          title: "Do you have any physical aliments prior to this session?",
        },
        {
          type: "nouislider",
          isRequired: true,
          name: "hoursOfSleep",
          title: "How many hours of restful sleep did you have last night?",
          rangeMin: 0,
          rangeMax: 12,
          defaultValue: 6,
          pipsText: [
            {
              value: 3,
              text: " ",
            },
            {
              value: 6,
              text: " ",
            },
            {
              value: 9,
              text: " ",
            },
          ],
        },
        {
          type: "checkbox",
          choices: [
            "Gluten",
            "Dairy",
            "Red meat",
            "White meat",
            "Seafood",
            "Avocado",
            "Bread",
            "Processed sugars",
            "Persimmon",
            "Pineapple",
            "Beef",
            "Coconut",
            "Watermelon",
          ],
          isRequired: true,
          name: "foodTypes",
          title: "In the last 48 hours, which food types have you consumed?",
        },
        {
          type: "matrixdropdown",
          name: "Feelings",
          title: "Select the responses that best capture your current outlook regarding your upcoming psilocybin session",
          isRequired: true,
          columns: [
            {
              name: "col1",
              cellType: "radiogroup",
              showInMultipleColumns: true,
              isRequired: true,
              choices: [
                "1",
                "2",
                "3",
                "4",
                "5",
              ],
            },
          ],
          rows: [
            "4.1 On a scale of 1 - 5, how excited are you?",
            "4.2 On a scale of 1 - 5, how scared are you?",
            "4.3 On a scale of 1 - 5, how fearful are you?",
          ],
        },
        // {
        //   type: "matrixdropdown",
        //   name: "Feelings2",
        //   title: "Please add any other emotions that capture your current outlook regarding your upcoming session",
        //   isRequired: true,
        //   columns: [
        //     {
        //       name: "col1",
        //       cellType: "radiogroup",
        //       showInMultipleColumns: true,
        //       isRequired: false,
        //       choices: [
        //         "1",
        //         "2",
        //         "3",
        //         "4",
        //         "5",
        //       ],
        //     },
        //   ],
        //   rows: [
        //     "5.1 On a scale of 1 - 5, how angry are you?",
        //   ],
        // },
      ],
    },
  ],
};

function sendDataToServer(survey) {
  const resultAsString = JSON.stringify(survey.data);
  alert(resultAsString);
}

widgets.nouislider(Survey);
const survey = new Survey.Model(surveyJSON);

survey.onComplete.add(function (result) {
  document.querySelector("#surveyResult").textContent =
    "Result JSON:\n" + JSON.stringify(result.data, null, 3);
});

survey.onUpdateQuestionCssClasses.add(function (survey, options) {
  const classes = options.cssClasses;

  classes.root = "sq-root";
  classes.title = "sq-title";
  classes.item = "sq-item";
  classes.label = "sq-label";

  if (options.question.isRequired) {
    classes.title += " sq-title-required";
    classes.root += " sq-root-required";
  }

  if (options.question.getType() === "checkbox") {
    classes.root += " sq-root-cb";
  }
});

function MayaSurvey() {
  useEffect(() => {
    const sqItems = document.querySelectorAll(".sq-item");
    const controlRadios = document.querySelectorAll(".sv_q_radiogroup_control_item");
    const questionsTitles = document.querySelectorAll(".sq-title");
    const sliders = document.querySelectorAll(".noUi-target");
    const checkboxItemsContainers = document.querySelectorAll(".sq-root-cb");
    const checkboxItems = [];
    const checkboxes = document.querySelectorAll(".sv_q_checkbox_control_item");
    const radiobuttons = document.querySelectorAll(".sv_q_radiogroup_inline");
    const allRows = document.querySelectorAll("tr");
    const questionRows = [];
    const questions = [];

    // console.log(controlRadios)
    // console.log(sqItems)

    allRows.forEach(row => {
      if (!row.firstChild.classList.contains('sv_matrix_cell_header')) {
        questionRows.push(row)
      }
    })

    questionRows.forEach(row => {
      const questionCell = row.childNodes[0];
      questions.push(questionCell);
      row.removeChild(questionCell)
      const newRow = document.createElement('tr')
      questionCell.setAttribute('colspan', 5)
      questionCell.classList.add('question_cell')
      questionCell.classList.remove('sv_matrix_cell')
      newRow.appendChild(questionCell)
      row.parentNode.insertBefore(newRow, row)
    })

    // const customQuestion = questions[3]

    // const customQuestionInputRow = document.createElement('tr')
    // const customQuestionInputCell = document.createElement('td')
    // customQuestionInputCell.setAttribute('colspan', 5)
    // const customInput = document.createElement('input')
    // customInput.classList.add('question_input')
    // customQuestionInputCell.appendChild(customInput)
    // customQuestionInputRow.appendChild(customQuestionInputCell)
    // customQuestion.parentNode.parentNode.insertBefore(customQuestionInputRow, customQuestion.parentNode)

    checkboxItemsContainers.forEach((container) =>
      container.childNodes.forEach(
        (child) =>
          child.classList.contains("sq-item") && checkboxItems.push(child)
      )
    );

    checkboxItems.forEach((item, i) => {
      item.addEventListener("click", (event) => {
        checkboxes[i].click();
      });
    });

    checkboxes.forEach((item, i) => {
      item.parentNode.addEventListener('click', () => {
        checkboxItems[i].click();
      })
    })

    radiobuttons.forEach((item, i) => {
      item.addEventListener("click", () => {
        item.firstChild.firstChild.click();
      });
      item.setAttribute("data-after", `${i % 5 + 1}`);
    })

    sqItems.forEach((item, i) => {
      if (item.parentNode.getAttribute("role") === "radiogroup") {
        item.addEventListener("click", () => {
          // controlRadios[i].click();
          item.firstChild.click()
        });
      }
    });

    questionsTitles.forEach((title, i) => {
      title.classList.add("sq-title-progress");
      title.setAttribute("data-before", `${i + 1}/${questionsTitles.length}`);
      if (i === 3) {
        title.style.marginBottom = '-40px'
      }
    });

    sliders[0].noUiSlider.updateOptions({
      format: {
        to: function (value) {
          return Math.round(value);
        },
        from: Number,
      },
    });

    return () => {
      sqItems.forEach((item, i) => {
        item.removeEventListener("click", () => {
          controlRadios[i].click();
        });
      });
      checkboxItems.forEach((item, i) => {
        item.removeEventListener("click", () => {
          checkboxes[i].click();
        });
      });
      radiobuttons.forEach((item, i) => {
        item.removeEventListener("click", (event) => {
          item.firstChild.firstChild.click();
        });
      })
    };
  }, []);

  return (
    <>
      <div id="surveyContainer">
        <Survey.Survey model={survey} onComplete={sendDataToServer} />
      </div>
    </>
  );
}

export default MayaSurvey;
