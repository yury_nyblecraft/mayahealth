# Survey for Maya Integration Instructions


  - Add assets (fonts and images) from this repo to your `assets` folder
  - Add files `MayaSurvey.js`, `MayaSurvey.css` to your project
  - Check routes for import fonts and images into `MayaSurvey.css` from `assets` folder
  - Check route for import `MayaSurvey.css` file into `MayaSurvey.js`
  - Write in terminal 
  ```
    npm i survey-react surveyjs-widgets jquery nouislider
  ```
  
  - In the file to integrate import `MayaSurvey` component from `MayaSurvey.js` and render it in the place where you need

### App launch

If you want to run app

  - Clone this repo
  - Open folder with the project and write in terminal 
  ```
    npm i && npm start
  ```
